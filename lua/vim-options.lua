-- Using 2 spaces for indentation
vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")

-- Set leader key
vim.g.mapleader = " "

vim.wo.number = true
vim.wo.relativenumber = true

-- sync system clipboard
vim.opt.clipboard = 'unnamedplus'

-- Diagnostic printing
vim.diagnostic.config({
    virtual_text = false,
    signs = true,
    underline = true,
    update_in_insert = true,
    severity_sort = true,
})
local signs = { Error = " ", Warn = " ", Hint = "󰍉 ", Info = " " }
for type, icon in pairs(signs) do
  local hls = "DiagnosticSign" .. type
  vim.fn.sign_define(hls, { text = icon, texthl = hls, numhl = "" })
end
