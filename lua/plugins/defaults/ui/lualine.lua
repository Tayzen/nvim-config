return {
	{
		"nvim-lualine/lualine.nvim",
		config = function()
			require("lualine").setup({
				options = {
					theme = "tokyonight",
					icons_enabled = true,
					section_separators = { left = '', right = '' },
					component_separators = { left = '', right = '' },
					-- section_separators = "",
					-- component_separators = "",
					disabled_filetypes = { "neo-tree" },
				},
				sections = {
					lualine_a = {'mode'},
					lualine_b = {'branch', 'diff', 'diagnostics'},
					lualine_c = {},
					lualine_x = {'filename'},
					lualine_y = {'progress'},
					lualine_z = {'location'}
				},
			})
		end,
	}
}
