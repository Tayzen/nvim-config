return {
	"xiyaowong/nvim-transparent",
	dependencies = { "akinsho/bufferline.nvim" },
	lazy = false,
	config = function()
		require("transparent").clear_prefix("BufferLine")
	end,
}
