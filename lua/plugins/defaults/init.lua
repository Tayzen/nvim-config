local bufferline = require("plugins/defaults/ui/bufferline")
local diagflow = require("plugins/defaults/ui/diagflow")
local lualine = require("plugins/defaults/ui/lualine")
local neotree = require("plugins/defaults/ui/neo-tree")
local noice = require("plugins/defaults/ui/noice")
local rainbowDelimiters = require("plugins/defaults/ui/rainbow-delimiter")
local theming = require("plugins/defaults/ui/theming")
local transparent = require("plugins/defaults/ui/transparent")
local whichkeys = require("plugins/defaults/ui/which-keys")
local trouble = require("plugins/defaults/ui/trouble")

local debugging = require("plugins/defaults/languages/debugging")
local lsp = require("plugins/defaults/languages/lsp")
local completion = require("plugins/defaults/languages/completion")
local nonels = require("plugins/defaults/languages/nonels")
local treesitter = require("plugins/defaults/languages/treesitter")
local neoconf = require("plugins/defaults/features/neoconf")

local autoclose = require("plugins/defaults/features/auto-close")
local autosession = require("plugins/defaults/features/auto-session")
local gitstuff = require("plugins/defaults/features/git-stuff")
local comment = require("plugins/defaults/features/comment")
local incRename = require("plugins/defaults/features/inc-rename")
local telescope = require("plugins/defaults/features/telescope")
local vimTmuxNavigator = require("plugins/defaults/features/vim-tmux-navigator")

return {
	bufferline,
	diagflow,
	lualine,
	neotree,
	telescope,
	theming,
	transparent,
	debugging,
	autoclose,
	autosession,
	gitstuff,
	treesitter,
	lsp,
	completion,
	comment,
	nonels,
	whichkeys,
	trouble,
	incRename,
	noice,
	rainbowDelimiters,
	vimTmuxNavigator,
	neoconf,
}
