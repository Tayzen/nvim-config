return {
  {
    "lukas-reineke/indent-blankline.nvim",
    opts = {
      indent = {
        char = "│",
        tab_char = "│",
      },
      scope = { enabled = false },
      exclude = {
        filetypes = {
          "help",
          "alpha",
          "dashboard",
          "neo-tree",
          "Trouble",
          "trouble",
          "lazy",
          "mason",
          "notify",
          "toggleterm",
          "lazyterm",
        },
      },
    },
    main = "ibl",
  },
  {
    "echasnovski/mini.indentscope",
    version = false, -- wait till new 0.7.0 release to put it back on semver
    -- opts = {
    --   -- symbol = "▏",
    --   symbol = "│",
    --   options = {
    --     try_as_border = true,
    --   },
    --   draw = {
    --     delay = 50,
    --     animation = require("mini.indentscope").gen_animation.none(),
    --   },
    -- },
    init = function()
      vim.api.nvim_create_autocmd("FileType", {
        pattern = {
          "help",
          "alpha",
          "dashboard",
          "neo-tree",
          "Trouble",
          "trouble",
          "lazy",
          "mason",
          "notify",
          "toggleterm",
          "lazyterm",
        },
        callback = function()
          vim.b.miniindentscope_disable = true
        end,
      })
    end,
    config = function ()
      local indentscope = require("mini.indentscope")
      indentscope.setup(
        {
          -- symbol = "▏",
          symbol = "│",
          options = {
            try_as_border = true,
          },
          draw = {
            delay = 50,
            animation = indentscope.gen_animation.none(),
          },
        }
      )
    end
  }
}