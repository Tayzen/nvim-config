-- return {}
return {
    "lmburns/lf.nvim",
    dependencies = {"akinsho/toggleterm.nvim"},
    config = function()
        -- This feature will not work if the plugin is lazy-loaded
        vim.g.lf_netrw = 1
        
        require("toggleterm").setup({
            shade_terminals = false,
        })
        
        require("lf").setup({
            default_file_manager = false,
            escape_quit = false,
            border = "rounded",
            winblend = 0, -- psuedotransparency level
        })

        vim.keymap.set("n", "<C-n>", "<Cmd>Lf<CR>")
    end,
}