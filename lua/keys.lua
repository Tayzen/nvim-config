local wk = require("which-key")

-- paste without overwriting
vim.keymap.set("v", "p", "P")

-- redo
vim.keymap.set("n", "U", "<C-r>")

-- movement
vim.keymap.set("n", "C-u", "<C-u>zz")
vim.keymap.set("n", "C-d", "<C-d>zz")
vim.keymap.set("n", "W", "5w")
vim.keymap.set("n", "B", "5b")

-- jump back and forward
vim.keymap.set("n", "C-H", "<C-o>")
vim.keymap.set("n", "C-L", "<C-i>")

-- clear search highlighting
vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<cr>")

-- skip folds (down, up)
vim.cmd("nmap j gj")
vim.cmd("nmap k gk")

-- Telescope
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<C-p>", builtin.find_files, {})

-- Neo-tree
vim.keymap.set("n", "<C-b>", "<cmd>Neotree toggle<cr>", {})

-- Which-key labels and keymaps
wk.add({
	-- No groups
	{ "<leader>a", hidden = true },
	{ "<leader>K", "<cmd>lua vim.lsp.buf.hover()<CR>", desc = "LSP hover" },
	{ "<leader>w", "<cmd>wall!<CR>", desc = "Save all files" },
	{ "<leader>Q", "<cmd>wqall!<CR>", desc = "Save and Quit" },
	{ "<leader>b", "<cmd>BufferLinePick<CR>", desc = "Buffer switcher" },

	-- Code
	{ "<leader>c", group = "+code" },
	{ "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc = "Code action" },
	{ "<leader>cf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", desc = "Format" },
	{ "<leader>cr", ":IncRename ", desc = "Rename" },
	{ "c", group = "+code" },
	{ "ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc = "Code action" },
	{ "cf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", desc = "Format" },
	{ "cr", ":IncRename ", desc = "Rename" },

	-- Debugger
	{ "<leader>d", group = "+debugger" },
	{ "<leader>dt", "<cmd>DapToggleBreakpoint<CR>", desc = "Toggle Breakpoint" },
	{ "<leader>dc", "<cmd>DapContinue<CR>", desc = "Continue" },
	{ "<leader>dx", "<cmd>DapTerminate<CR>", desc = "Terminate" },
	{ "<leader>do", "<cmd>DapStepOver<CR>", desc = "Step Over" },

	-- Find
	{ "<leader>f", group = "+find" },
	{ "<leader>ff", "<cmd>lua require('telescope.builtin').find_files()<CR>", desc = "Find Files" },
	{ "<leader>fg", "<cmd>lua require('telescope.builtin').live_grep()<CR>", desc = "Live Grep" },
	{ "<leader>fb", "<cmd>lua require('telescope.builtin').buffers()<CR>", desc = "Buffers" },
	{ "<leader>fh", "<cmd>lua require('telescope.builtin').help_tags()<CR>", desc = "Help Tags" },

	-- Goto
	{ "<leader>g", group = "+goto" },
	{ "<leader>ga", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc = "Code Action" },
	{ "<leader>gh", "<cmd>lua vim.lsp.buf.hover()<CR>", desc = "Hover" },
	{ "<leader>gd", "<cmd>lua vim.lsp.buf.definition()<CR>", desc = "Definition" },
	{ "<leader>gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", desc = "Declaration" },
	{ "<leader>gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", desc = "Implementation" },
	{ "<leader>gr", "<cmd>lua vim.lsp.buf.references()<CR>", desc = "References" },
	{ "g", group = "+goto" },
	{ "ga", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc = "Code Action" },
	{ "gh", "<cmd>lua vim.lsp.buf.hover()<CR>", desc = "Hover" },
	{ "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", desc = "Definition" },
	{ "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", desc = "Declaration" },
	{ "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", desc = "Implementation" },
	{ "gr", "<cmd>lua vim.lsp.buf.references()<CR>", desc = "References" },

	-- LSP
	{ "<leader>l", group = "+lsp" },
	{ "<leader>lf", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", desc = "Format" },
	{ "<leader>lr", ":IncRename ", desc = "Rename" },

	-- Trouble
	{ "<leader>x", group = "+trouble" },
	{ "<leader>xx", "<cmd>TroubleToggle<CR>", desc = "Toggle" },
	{ "<leader>xw", "<cmd>TroubleToggle workspace_diagnostics<CR>", desc = "Workspace Diagnostics" },
	{ "<leader>xd", "<cmd>TroubleToggle document_diagnostics<CR>", desc = "Document Diagnostics" },
	{ "<leader>xq", "<cmd>TroubleToggle quickfix<CR>", desc = "Quickfix" },
	{ "<leader>xl", "<cmd>TroubleToggle loclist<CR>", desc = "Loclist" },
})
