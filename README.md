# Nvim config / quickstart

![Screenshot of Neovim](./Screenshot.png)

A NeoVim configuration, made to be easy to use and adapt and also kind of minimal and fast.

## Reusable

I made my config to be very reusable for anyone. The plugin folder is divided into 2 main parts: the `default` folder and lots of other files configuring plugins. If you want to start quickly your configuration, you can delete all the individual plugin files outside of `default` and you're good to go. From that point you will still have your LSP working, a standard code completion and a very nice UI out of the box.

## Easy for beginners

If you are a beginner, you will be thrilled to learned that all the major keybindings are shown dynamically when you hit a key.

You don't know how to perform an action? Don't worry, almost every useful keybindings are starting with the space bar (the leader key).

If the default keybindings are not your cup of tea, no problem. Their configurations are available in their own file, no need to dig into the plugins' configuration to change the shortcut to open the file explorer for example.

## Quick

Thanks to the Lazy Package Manager which lazy loads the plugins the startup time is very fast (88ms on my M2 MacBook Air with 16GB RAM).

## Plugins

### Default configuration

- [`auto-session`](https://github.com/rmagatti/auto-session): Automatically save and load sessions
- [`autoclose.nvim`](https://github.com/m4xshen/autoclose.nvim): Automatically close pairs of brackets
- [`Comment.nvim`](https://github.com/numToStr/Comment.nvim): Toggle comments for lines or blocks of code
- [`git-signs.nvim`](https://github.com/lewis6991/gitsigns.nvim): Show git changes in the gutter
- [`vim-fugitive`](https://github.com/tpope/vim-fugitive): Git integration (to remove?)
- [`inc-rename`](https://github.com/smjonas/inc-rename.nvim): Rename variables using the LSP
- [`neoconf`](https://github.com/folke/neoconf.nvim): LSP configuration with `.vscode/settings.json`
- [`telescope.nvim`](https://github.com/nvim-telescope/telescope.nvim): Search files, grep, buffers, etc.
- [`vim-tmux-navigator`](https://github.com/christoomey/vim-tmux-navigator): Tmux integration
- [`nvim-cmp`](https://github.com/hrsh7th/nvim-cmp): Autocompletion
- [`LuaSnip`](https://github.com/L3MON4D3/LuaSnip): Snippets
- [`friendly-snippets`](https://github.com/rafamadriz/friendly-snippets): Snippets collection
- [`nvim-lspconfig`](https://github.com/neovim/nvim-lspconfig): LSP configuration
- [`lsp-zero.nvim`](https://github.com/VonHeikemen/lsp-zero.nvim): Easy integration between LSPs and Completion
- [`mason.nvim`](https://github.com/williamboman/mason.nvim): LSP installer
- [`mason-lspconfig.nvim`](https://github.com/williamboman/mason-lspconfig.nvim): Bridge between Mason and LSP config
- [`dap`](https://github.com/mfussenegger/nvim-dap): Debugger integration
- [`dap-ui`](https://github.com/rcarriga/nvim-dap-ui): Debugging UI
- [`mason-nvim-dap.nvim`](https://github.com/jay-babu/mason-nvim-dap.nvim): Bridge between Mason and DAP
- [`none-ls.nvim`](https://github.com/nvimtools/none-ls.nvim): Integration for linters and formatters
- [`treesitter`](https://github.com/nvim-treesitter/nvim-treesitter): Syntax highlighting (and more!)
- [`bufferline.nvim`](https://github.com/akinsho/bufferline.nvim): Display buffers as tabs
- [`diagflow`](https://github.com/dgagn/diagflow.nvim): Another way to show LSP diagnostics
- [`lualine.nvim`](https://github.com/nvim-lualine/lualine.nvim): Status line
- [`neo-tree.nvim`](https://github.com/nvim-neo-tree/neo-tree.nvim): File explorer
- [`noice.nvim`](https://github.com/folke/noice.nvim): Lots of UI improvements
- [`rainbow-delimiters.nvim`](https://github.com/p00f/nvim-ts-rainbow): Rainbow parentheses
- [`tokyonight`](https://github.com/folke/tokyonight.nvim): Color scheme
- [`trouble.nvim`](https://github.com/folke/trouble.nvim): Show diagnostics
- [`which-key.nvim`](https://github.com/folke/which-key.nvim): Show keybindings while typing

### Custom

- [`codeium.vim`](https://github.com/Exafunction/codeium.vim): Codeium integration
- [`flash.nvim`](https://github.com/folke/flash.nvim): Easily jump anywhere in the code by typing 2 letters
- [`harpoon`](https://github.com/ThePrimeagen/harpoon/tree/harpoon2): Manage bookmarks
- [`indent-blankline.nvim`](https://github.com/lukas-reineke/indent-blankline.nvim): Show indentation lines
- [`illuminate.nvim`](https://github.com/RRethy/vim-illuminate): Highlight other uses of the word under the cursor
- [`lf.nvim`](https://github.com/lmburns/lf.nvim): LF file manager integration
- [`nvim-scrollbar`](https://github.com/petertriho/nvim-scrollbar): Scrollbar
- [`spectre.nvim`](https://github.com/nvim-pack/nvim-spectre): Find and replace in the whole project
- [`startuptime`](https://github.com/dstein64/vim-startuptime): Show startup time
- [`todo-comments.nvim`](https://github.com/folke/todo-comments.nvim): Show TODO comments
- [`venv-selector.nvim`](https://github.com/linux-cultist/venv-selector.nvim): Select Python's virtualenvs
